package main

import (
	"flag"
	"fmt"
	"github.com/alexmullins/zip"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
	"xzip/bfpass"
	"xzip/lreader"
)

var (
	wg        sync.WaitGroup
	listFile  string
	fileName  string
	length    int
	startTime = time.Now()
)

func init() {
	flag.StringVar(&listFile, "list", "", "")
	flag.IntVar(&length, "len", 3, "")
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	flag.Parse()
	args := flag.Args()

	if len(args) < 1 {
		fmt.Println("請指定檔案名稱\n")
		printGuide()
		os.Exit(0)
	}

	fileName = args[0]

	if !checkFile() {
		fmt.Println("無法開啟壓縮檔，請確認檔名及路徑正確")
		os.Exit(0)
	}

	if listFile != "" {
		runByListFile()
	} else {
		runByBruteForce()
	}

	fmt.Printf("執行完畢，查無密碼。消耗時間：%s\n", time.Since(startTime))
}

func checkFile() bool {
	zipReader, err := zip.OpenReader(fileName)
	if err != nil {
		return false
	}
	zipReader.Close()
	return true
}

func runByBruteForce() {
	password := strings.Repeat("0", length)
	end := strings.Repeat("z", length)

	testPass := func(password string) {
		wg.Add(1)
		defer wg.Done()

		zipReader, _ := zip.OpenReader(fileName)
		defer zipReader.Close()
		f := zipReader.File[0]

		err := openWithPass(f, password)
		if err == nil {
			printResult(password)
			os.Exit(1)
		}
	}

	for password != end {
		go testPass(password)
		password = bfpass.NextPassword(password)
	}
	testPass(password)

	wg.Wait()
}

func runByListFile() {
	lines, err := lreader.ReadLines(listFile)
	if err != nil {
		fmt.Println("讀取密碼檔時發生錯誤")
	}

	for _, line := range lines {
		go func(password string) {
			wg.Add(1)
			defer wg.Done()
			zipReader, err := zip.OpenReader(fileName)
			defer zipReader.Close()
			f := zipReader.File[0]
			err = openWithPass(f, password)
			if err == nil {
				printResult(password)
				os.Exit(1)
			}
		}(line)
	}

	wg.Wait()
}

func openWithPass(file *zip.File, password string) error {
	file.SetPassword(password)
	//fmt.Println("checking...", password)
	_, err := file.Open()
	return err
}

func printGuide() {
	fmt.Println("使用說明")
	fmt.Println("暴力破解：xzip [-len 密碼長度] 檔名")
	fmt.Println("密碼表破解：xzip [-list 密碼表路徑] 檔名")
}

func printResult(password string) {
	fmt.Printf("找到密碼，破解成功！密碼為：%s 消耗時間：%s\n", password, time.Since(startTime))
}
