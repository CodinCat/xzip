package bfpass

import (
	"bytes"
)

const (
	c0 uint8 = 48
	c9 uint8 = 57
	cA uint8 = 65
	cZ uint8 = 90
	ca uint8 = 97
	cz uint8 = 122
)

func NextPassword(str string) string {
	var buffer bytes.Buffer
	var next string
	end := false
	i := 0

	first, end := nextChar(str[i])
	buffer.WriteString(first)

	for end {
		i = i + 1
		next, end = nextChar(str[i])
		buffer.WriteString(next)
	}

	return buffer.String() + str[i+1:]
}

func nextChar(c uint8) (string, bool) {
	if c == c9 {
		return "A", false
	} else if c == cZ {
		return "a", false
	} else if c == cz {
		return "0", true
	} else {
		return string(c + 1), false
	}
}